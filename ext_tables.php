<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Almoe.Secdownload',
            'Amsecdownload',
            'Secure Download'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('secdownload', 'Configuration/TypoScript', 'Almoe Secure File Download');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_secdownload_domain_model_download', 'EXT:secdownload/Resources/Private/Language/locallang_csh_tx_secdownload_domain_model_download.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_secdownload_domain_model_download');

    }
);
