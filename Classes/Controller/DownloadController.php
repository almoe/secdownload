<?php
namespace Almoe\Secdownload\Controller;

/***
 *
 * This file is part of the "Almoe Secure File Download" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Sebastian Albers <willkommen@almoe.de>, almö Medienagentur GmbH
 *
 ***/

/**
 * DownloadController
 */
class DownloadController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * fileRepository
     *
     * @var \Almoe\Secdownload\Domain\Repository\FileRepository
     * @inject
     */
    protected $fileRepository = NULL;

    /**
     * action form
     *
     * @return void
     */
    public function formAction()
    {

        $fileIdentifier = $this->request->hasArgument('file') ? $this->request->getArgument('file') : null;




        $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();
        $storage = $resourceFactory->getDefaultStorage();
        $file = $storage->getFile($fileIdentifier);


        header('Content-Description: File Transfer');
        header('Content-Type: ' . $file->getMimeType());
        header('Content-Disposition: attachment; filename=' . $file->getName());
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . $file->getSize());

        ob_clean();
        flush();
        echo $file->getContents();
        //ATTENTION: This exit is very important when you want to use the plugin more than one time per page,
        //because for every instance of the plugin, this action will be executed
        //and therefore multiple userData objects will be created
        exit;
    }

    /**
     * action download
     *
     * @return void
     */
    public function downloadAction()
    {

    }
}
