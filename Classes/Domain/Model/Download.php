<?php
namespace Almoe\Secdownload\Domain\Model;

/***
 *
 * This file is part of the "Almoe Secure File Download" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Sebastian Albers <willkommen@almoe.de>, almö Medienagentur GmbH
 *
 ***/

/**
 * Download
 */
class Download extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    }
