<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Almoe.Secdownload',
            'Amsecdownload',
            [
                'Download' => 'form, download'
            ],
            // non-cacheable actions
            [
                'Download' => 'form, download'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    amsecdownload {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('secdownload') . 'Resources/Public/Icons/user_plugin_amsecdownload.svg
                        title = LLL:EXT:secdownload/Resources/Private/Language/locallang_db.xlf:tx_secdownload_domain_model_amsecdownload
                        description = LLL:EXT:secdownload/Resources/Private/Language/locallang_db.xlf:tx_secdownload_domain_model_amsecdownload.description
                        tt_content_defValues {
                            CType = list
                            list_type = secdownload_amsecdownload
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
