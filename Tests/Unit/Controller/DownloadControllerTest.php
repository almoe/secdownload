<?php
namespace Almoe\Secdownload\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Sebastian Albers <willkommen@almoe.de>
 */
class DownloadControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Almoe\Secdownload\Controller\DownloadController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Almoe\Secdownload\Controller\DownloadController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
