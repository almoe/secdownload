<?php
namespace Almoe\Secdownload\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Sebastian Albers <willkommen@almoe.de>
 */
class DownloadTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Almoe\Secdownload\Domain\Model\Download
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Almoe\Secdownload\Domain\Model\Download();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
