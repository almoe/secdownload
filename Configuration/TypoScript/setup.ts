
plugin.tx_secdownload_amsecdownload {
    view {
        templateRootPaths.0 = EXT:secdownload/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_secdownload_amsecdownload.view.templateRootPath}
        partialRootPaths.0 = EXT:secdownload/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_secdownload_amsecdownload.view.partialRootPath}
        layoutRootPaths.0 = EXT:secdownload/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_secdownload_amsecdownload.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_secdownload_amsecdownload.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_secdownload._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-secdownload table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-secdownload table th {
        font-weight:bold;
    }

    .tx-secdownload table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)



downloadPDF = PAGE
downloadPDF {
    typeNum = 79991

    # add plugin
    10 < tt_content.list.20.secdownload_amsecdownload
    # disable header code
    config {
        disableAllHeaderCode = 1
        xhtml_cleaning = 0
        admPanel = 0
        debug = 0
        index_enable = 0
        removeDefaultJS = 1
        removeDefaultCss = 1
        removePageCss = 1
        no_cache = 1
    }
}