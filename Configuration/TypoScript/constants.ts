
plugin.tx_secdownload_amsecdownload {
    view {
        # cat=plugin.tx_secdownload_amsecdownload/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:secdownload/Resources/Private/Templates/
        # cat=plugin.tx_secdownload_amsecdownload/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:secdownload/Resources/Private/Partials/
        # cat=plugin.tx_secdownload_amsecdownload/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:secdownload/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_secdownload_amsecdownload//a; type=string; label=Default storage PID
        storagePid =
    }
}
